import requests
import json
import os
import boto3
import tempfile
from botocore.exceptions import ClientError
import logging

def getWeather(w_url, apikey, s3_bucket='tatum-web' , city='ranchi',):
    url = w_url + "?q=" + city + "&appid=" +apikey
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    weather_dict = json.loads(response.text)


    json_data_list = [];
    data = {}
    data['id'] = weather_dict['weather'][0]['id']
    data['name'] = weather_dict['name']
    data['timezone'] = weather_dict['timezone']
    data['main'] = weather_dict['weather'][0]['main']
    data['temp'] = weather_dict['main']['temp']
    data['feels_like'] = weather_dict['main']['feels_like']
    data['humidity'] = weather_dict['main']['humidity']
    data['country'] = weather_dict['sys']['country']
    json_data = json.dumps(data)
    print(json_data)
    json_data_list.append(data)
    print(json.dumps(json_data_list))

    s3_data = "var mydata = "
    s3_data += json.dumps(json_data_list)
    s3_data += ";"

    filename = tempfile.NamedTemporaryFile()
    f = open(filename.name, "a")
    f.write(s3_data)
    f.close()
    print(s3_data)
    upload_file(filename.name,s3_bucket)


def upload_file(file_name, bucket, object_name='data.js'):


    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(file_name)

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

url = os.environ['API_URL']
city = os.environ['CITY']
apikey = os.environ['API_KEY']
s3_bucket = os.environ['S3_BUCKET']
getWeather(url, apikey , s3_bucket, city)