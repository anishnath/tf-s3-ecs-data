# ABOUT

Create INFRA such as an scheduled job which can run ECS that can pull some weather API and store the result in the S3 for static website demo 

# DEMO 

https://d2148lzr7yqrzd.cloudfront.net/

# FLOW
Using Gitlab CI/CD to automatically Create ECS INFRA. The ECS INFRA is based out of 

![](01.png)

Using Gitlab CI/CD to automatically Create S3 INFRA

![](02.png)

Using Gitlab CI/CD to automatically build and push a new docker image on HUB and deploy it to ECS schedule JOB

![](03.png)


## INFRA 

### ECS 

```cd infra/ecs```
**Commands to run**
- cd infra/ecs
- terraform init - To initialise the project and download any required packages.
- terraform plan - To see what has changed compared to your current configuration.
- terrform apply - To apply your changes.

## INFRA

### S3

```cd infra/s3/```
**Commands to run**
- cd infra/s3/
- terraform init - To initialise the project and download any required packages.
- terraform plan - To see what has changed compared to your current configuration.
- terrform apply - To apply your changes.


### Docker

```cd www```

*To Build*

```aidl
docker build -t anishnath/tf-ecs-s3-cron  .
```

*Running locally*
```
docker run -e API_KEY=XXXXX -e AWS_ACCESS_KEY_ID=YYYY*** -e AWS_SECRET_ACCESS_KEY=ZZZZ***** -e AWS_REGION=us-east-1  anishnath/tf-ecs-s3-cron
```
**TODO**

- Cloudfront - Cache Invalidation once data is uploaded to S3
- Route53 entry for domain
- ACME SSL DNS Validation
- SECURITY BUCKET PERMISSIONS
- ROLE BASED APPROACH
- PIPELINE RUNNER-TAG
- TERRAFORM S3 STATE FILE

**IMPROVEMENT**

This cron functionality from ECS should be Moved to Lambda a much convienet way 

